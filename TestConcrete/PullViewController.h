//
//  PullViewController.h
//  TestConcrete
//
//  Created by Daniel Maia dos Passos on 10/01/17.
//  Copyright © 2017 Daniel Maia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ServiceListener.h"
#import "PullRequest.h"

@interface PullViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, ServiceListener>

@property (weak, nonatomic) IBOutlet UITableView *table;
- (IBAction)clickBtnTryAgain:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnTryAgain;


@property (strong, nonatomic) NSString *repoName;
@property (strong, nonatomic) NSMutableArray<PullRequest *> *list;

@end
