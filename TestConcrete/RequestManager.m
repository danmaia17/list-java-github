//
//  RequestManager.m
//  TestConcrete
//
//  Created by Daniel Maia dos Passos on 10/01/17.
//  Copyright © 2017 Daniel Maia. All rights reserved.
//

#import "RequestManager.h"
#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <Foundation/NSJSONSerialization.h>
#import "AFNetworking.h"

@implementation RequestManager {
    NSHTTPURLResponse* connectionResponse;
    NSMutableData* connectionData;
    NSError* connectionError;

}

- (id) initWithListener:(id<RequestListener>)requestListener {
    self = [super init];
    if (self) {
        connectionData = [[NSMutableData alloc]init];
        listener = requestListener;
    }
    return self;
}

- (void)getFromURL: (NSString*)urlGET {
    NSURL *url = [NSURL URLWithString:urlGET];
    NSLog(@"URL: %@", url);
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [[manager dataTaskWithRequest:request completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error){
        
        if (!error) {
            [listener onRequestComplete:responseObject];

        } else {
            [listener onRequestError:response WithData:responseObject WithError:error];
        }
    }] resume];
}



@end
