//
//  RequestManager.h
//  TestConcrete
//
//  Created by Daniel Maia dos Passos on 10/01/17.
//  Copyright © 2017 Daniel Maia. All rights reserved.
//

#import "RequestListener.h"
#import "JSONModel.h"


@interface RequestManager : NSObject<NSURLConnectionDataDelegate, NSURLConnectionDelegate>
{
    id<RequestListener> listener;
    bool autenticar;
}
@property Class classeRetorno;

- (void)getFromURL: (NSString*)urlGET;
- (id)initWithListener:(id <RequestListener>)listener;



@end
