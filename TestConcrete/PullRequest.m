//
//  PullRequest.m
//  TestConcrete
//
//  Created by Daniel Maia dos Passos on 10/01/17.
//  Copyright © 2017 Daniel Maia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PullRequest.h"

@implementation PullRequest

@synthesize title;
@synthesize body;
@synthesize html_url;
@synthesize user;

@end
