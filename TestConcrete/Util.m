//
//  ClasseUtil.m
//  TestConcrete
//
//  Created by Daniel Maia dos Passos on 10/01/17.
//  Copyright © 2017 Daniel Maia. All rights reserved.
//


#import <Foundation/Foundation.h>
#import "Util.h"
#import "Reachability.h"
#import "MBProgressHUD.h"

@implementation Util

+(BOOL)internetConnected{
    if ([[Reachability reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable){
        return NO;
    }else{
        return YES;
    }
}

+(void)showLoading:(UIView *)view{
    MBProgressHUD *carregando = [MBProgressHUD showHUDAddedTo:view animated:YES];
    [carregando setLabelText:NSLocalizedString(@"Carregando", nil)];
}

+(void)hiddenLoading:(UIView *)view{
    [MBProgressHUD hideHUDForView:view animated:YES];
}

+(void)exibirAlerta:(NSString*)titulo mensagem:(NSString*)mensagem {
    UIAlertView *alert;
    alert = [[UIAlertView alloc] initWithTitle:titulo message:mensagem delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil, nil];
    [alert  show];
}


@end

















