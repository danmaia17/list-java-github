//
//  Owner.h
//  TestConcrete
//
//  Created by Daniel Maia dos Passos on 10/01/17.
//  Copyright © 2017 Daniel Maia. All rights reserved.
//

#import "JSONModel.h"
#import <Foundation/Foundation.h>

@interface Owner : JSONModel

@property (strong, nonatomic) NSString *login;
@property (strong, nonatomic) NSString <Optional> *avatar_url;

@end
