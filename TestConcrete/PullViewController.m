//
//  PullViewController.m
//  TestConcrete
//
//  Created by Daniel Maia dos Passos on 10/01/17.
//  Copyright © 2017 Daniel Maia. All rights reserved.
//

#import "PullViewController.h"
#import "Reachability.h"
#import "GithubService.h"
#import "PullRequest.h"
#import "UIImageView+WebCache.h"
#import "ItemPullTableViewCell.h"
#import "PullRequest.h"
#import "SVPullToRefresh.h"
#import "MBProgressHUD.h"
#import "Util.h"

#define HEIGHT_CELL 131;

@interface PullViewController ()

@end

@implementation PullViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _table.delegate = self;
    _table.dataSource = self;
    _list = [[NSMutableArray alloc] init];
    _table.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    _table.tableFooterView.backgroundColor = [UIColor colorWithRed:235/255.0 green:235/255.0 blue:241/255.0 alpha:1.0];
    
    [self.navigationItem setTitle:_repoName];
    [self adjustControlVisibility:YES];
    [self getData];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    self.navigationController.navigationBar.translucent = NO;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    _btnTryAgain.hidden = YES;
    
    [self.navigationController.navigationBar setBarTintColor:[UIColor blackColor]];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    [self.navigationController.navigationBar setBarStyle:UIBarStyleBlack];
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    
    UIBarButtonItem *barButton = [[UIBarButtonItem alloc] init];
    barButton.title = @"";
    self.navigationController.navigationBar.topItem.backBarButtonItem = barButton;
    
    [_table addPullToRefreshWithActionHandler:^{
        [self getData];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return HEIGHT_CELL;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _list.count;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    PullRequest * p = [_list objectAtIndex:indexPath.row];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:p.html_url]];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    PullRequest * pull;
    
    [_table registerNib:[UINib nibWithNibName:@"ItemPullCell" bundle:nil]  forCellReuseIdentifier:@"cellPull"];
    ItemPullTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"cellPull" forIndexPath:indexPath];
    
    pull = [_list objectAtIndex:indexPath.row];
    [self configCell:cell pull:pull];
    
    return cell;
}

-(void)configCell:(ItemPullTableViewCell *)c pull:(PullRequest *)pull{
    c.lblTitle.text = pull.title;
    c.lblUserName.text = pull.user.login;
    
    if (pull.body.length > 0){
        c.lblDesc.text = pull.body;
        c.lblDesc.textColor =  [UIColor blackColor];
    }else {
        c.lblDesc.text = @"Sem descrição ..";
        c.lblDesc.textColor = [UIColor lightGrayColor];
    }
    
    [c.imgAvatar sd_setImageWithURL:[NSURL URLWithString:pull.user.avatar_url] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        if (error) {
            c.imgAvatar.image = [UIImage imageNamed:@"icon_profile_empty"];
        }
    }];
}

-(void)getData{
    if ([Util internetConnected]){
        [Util showLoading:self.view];
        
        GithubService *service = [[GithubService alloc] initWithServiceListener:self];
        [service getPullRequest:_repoName];
    } else {
        [Util exibirAlerta:@"Ops.." mensagem:@"Verifique a conexão com a internet"];
        [self adjustControlVisibility:NO];
    }
}

-(void)onSuccess:(id)objetoRetorno{
    NSArray* models = [PullRequest arrayOfModelsFromDictionaries: objetoRetorno[0] error:nil];
    _list = [models copy];
    
    [self adjustControlVisibility:YES];
    [Util hiddenLoading:self.view];
    [_table.pullToRefreshView stopAnimating];
    [_table reloadData];
}

-(void)onError:(NSHTTPURLResponse*)response WithData:(NSData*)data WithError:(NSError*)error{
    [Util hiddenLoading:self.view];
    [Util exibirAlerta:@"Ops.." mensagem:@"Erro ao buscar os dados no servidor!"];
    [self adjustControlVisibility:NO];
    NSLog(@"Erro!");
}

- (IBAction)clickBtnTryAgain:(id)sender {
    [self adjustControlVisibility:YES];
    [self getData];
}

-(void)adjustControlVisibility:(BOOL)showTable{
    if (showTable){
        _table.hidden = NO;
        _btnTryAgain.hidden = YES;
    } else {
        _table.hidden = YES;
        _btnTryAgain.hidden = NO;
    }
}


@end


























