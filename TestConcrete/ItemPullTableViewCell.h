//
//  ItemPullTableViewCell.h
//  TestConcrete
//
//  Created by Daniel Maia dos Passos on 10/01/17.
//  Copyright © 2017 Daniel Maia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ItemPullTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *vwMain;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblDesc;
@property (weak, nonatomic) IBOutlet UIImageView *imgAvatar;
@property (weak, nonatomic) IBOutlet UILabel *lblUserName;

@end
