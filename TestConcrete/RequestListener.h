//
//  RequestListener.h
//  TestConcrete
//
//  Created by Daniel Maia dos Passos on 10/01/17.
//  Copyright © 2017 Daniel Maia. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol RequestListener <NSObject>

- (void) onRequestComplete:(id) responseObject;
- (void) onRequestError:(NSURLResponse *)response WithData:(NSData*)data WithError:(NSError*)error;
@end
