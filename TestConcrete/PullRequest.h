//
//  PullRequest.h
//  TestConcrete
//
//  Created by Daniel Maia dos Passos on 10/01/17.
//  Copyright © 2017 Daniel Maia. All rights reserved.
//

#import "JSONModel.h"
#import "User.h"
#import <Foundation/Foundation.h>

@interface PullRequest : JSONModel

@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSString *body;
@property (strong, nonatomic) NSString *html_url;
@property (strong, nonatomic) User *user;

@end
