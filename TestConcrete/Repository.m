//
//  Repository.m
//  TestConcrete
//
//  Created by Daniel Maia dos Passos on 10/01/17.
//  Copyright © 2017 Daniel Maia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Repository.h"

@implementation Repository

@synthesize owner;
@synthesize name;
@synthesize description;
@synthesize stargazers_count;
@synthesize forks_count;

@end
