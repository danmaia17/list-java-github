//
//  ItemListTableViewCell.h
//  TestConcrete
//
//  Created by Daniel Maia dos Passos on 10/01/17.
//  Copyright © 2017 Daniel Maia. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ItemListTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblNameRepository;
@property (weak, nonatomic) IBOutlet UILabel *lblDescRepository;
@property (weak, nonatomic) IBOutlet UILabel *lblCountForksRepository;
@property (weak, nonatomic) IBOutlet UILabel *lblCountStarsRepository;

@property (weak, nonatomic) IBOutlet UIImageView *imgAvatarUser;
@property (weak, nonatomic) IBOutlet UILabel *lblNameUser;

@end
