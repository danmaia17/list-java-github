//
//  ItemPullTableViewCell.m
//  TestConcrete
//
//  Created by Daniel Maia dos Passos on 10/01/17.
//  Copyright © 2017 Daniel Maia. All rights reserved.
//

#import "ItemPullTableViewCell.h"

@implementation ItemPullTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
