//
//  githubService.h
//  TestConcrete
//
//  Created by Daniel Maia dos Passos on 10/01/17.
//  Copyright © 2017 Daniel Maia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseService.h"
#import "Repository.h"

@class Repository;


@interface GithubService : BaseService

-(void)getJavaData: (int)page;
-(void)getPullRequest: (NSString *)name;

@end
