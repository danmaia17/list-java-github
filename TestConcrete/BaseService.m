//
//  BaseService.m
//  TestConcrete
//
//  Created by Daniel Maia dos Passos on 10/01/17.
//  Copyright © 2017 Daniel Maia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseService.h"
#import <UIKit/UIKit.h>
#import "FTHTTPCodes.h"


@implementation BaseService

-(id)initWithServiceListener:(id<ServiceListener>)serviceListener{
    self = [self init];
    if(self){
        listener = serviceListener;
    }
    return self;
}

-(void)onRequestError:(NSHTTPURLResponse*)response WithData:(NSData*)data WithError:(NSError*)error{
    [listener onError:response WithData:data WithError:error];
}
-(void)onRequestComplete:(id)responseObject{
}

@end
