//
//  BaseService.h
//  TestConcrete
//
//  Created by Daniel Maia dos Passos on 10/01/17.
//  Copyright © 2017 Daniel Maia. All rights reserved.
//

#import "RequestManager.h"
#import "ServiceListener.h"

@interface BaseService : NSObject<RequestListener>
{
    id<ServiceListener> listener;
}

-(id)initWithServiceListener:(id<ServiceListener>)serviceListener;
@end
