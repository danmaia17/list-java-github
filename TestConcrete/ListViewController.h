//
//  ListViewController.h
//  TestConcrete
//
//  Created by Daniel Maia dos Passos on 10/01/17.
//  Copyright © 2017 Daniel Maia. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ServiceListener.h"
#import "Repository.h"

@interface ListViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, ServiceListener>

@property (weak, nonatomic) IBOutlet UITableView *table;
@property (strong, nonatomic) NSMutableArray<Repository *> *list;

- (IBAction)clickBtnTryAgain:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnTryAgain;


@end
