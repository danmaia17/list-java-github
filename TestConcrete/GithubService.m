//
//  githubService.m
//  TestConcrete
//
//  Created by Daniel Maia dos Passos on 10/01/17.
//  Copyright © 2017 Daniel Maia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Response.h"
#import "PullRequest.h"
#import "GithubService.h"
#import "RequestManager.h"

@implementation GithubService {
    Class retorno;
    BOOL ePerfilCondomino;
}

-(void)getJavaData:(int)page{
    NSString * url = [NSString stringWithFormat: @"https://api.github.com/search/repositories?q=language:Java&sort=stars&page=%i", page];
    
    RequestManager * manager = [[RequestManager alloc] initWithListener:self];
    retorno = [Response class];
    [manager getFromURL:url];
}

-(void)getPullRequest:(NSString *)name{
    NSString * url = [NSString stringWithFormat: @"https://api.github.com/repos/%@/pulls", name];
    
    RequestManager * manager = [[RequestManager alloc] initWithListener:self];
    retorno = [PullRequest class];
    [manager getFromURL:url];
    
}

-(void)onRequestComplete:(id)responseObject{
    NSMutableArray *array = [[NSMutableArray alloc] init];
    [array addObject:responseObject];
    NSArray* models = [retorno arrayOfModelsFromDictionaries: [array copy] error:nil];
    
    if (models)
        [listener onSuccess:models];
    else
        [listener onSuccess:array];
}


@end

