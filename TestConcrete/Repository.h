//
//  Repository.h
//  TestConcrete
//
//  Created by Daniel Maia dos Passos on 10/01/17.
//  Copyright © 2017 Daniel Maia. All rights reserved.
//

#import "JSONModel.h"
#import "Owner.h"
#import <Foundation/Foundation.h>

@interface Repository : JSONModel


@property (strong, nonatomic) Owner *owner;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *full_name;
@property (strong, nonatomic) NSString <Optional> *description;
@property (nonatomic) int stargazers_count;
@property (nonatomic) int forks_count;



@end
