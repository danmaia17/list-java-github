//
//  Response.m
//  TestConcrete
//
//  Created by Daniel Maia dos Passos on 10/01/17.
//  Copyright © 2017 Daniel Maia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Response.h"

@implementation Response

@synthesize total_count;
@synthesize incomplete_results;
@synthesize items;

@end
