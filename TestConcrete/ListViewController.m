//
//  ListViewController.m
//  TestConcrete
//
//  Created by Daniel Maia dos Passos on 10/01/17.
//  Copyright © 2017 Daniel Maia. All rights reserved.
//

#import "ListViewController.h"
#import "ItemListTableViewCell.h"
#import "Reachability.h"
#import "GithubService.h"
#import "Repository.h"
#import "Response.h"
#import "UIImageView+WebCache.h"
#import "SVPullToRefresh.h"
#import "PullViewController.h"
#import "Util.h"


#define HEIGHT_CELL 104;

@interface ListViewController ()

@end

@implementation ListViewController{
    int page;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _table.delegate = self;
    _table.dataSource = self;
    _list = [[NSMutableArray alloc] init];

    [self.navigationItem setTitle:NSLocalizedString(@"Github JavaPop", nil)];
    [self adjustControlVisibility:YES];
    [self getData:NO];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    self.navigationController.navigationBar.translucent = NO;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self.navigationController.navigationBar setBarTintColor:[UIColor blackColor]];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    [self.navigationController.navigationBar setBarStyle:UIBarStyleBlack];
    
    [_table addPullToRefreshWithActionHandler:^{
        [self getData:NO];
    }];
    
    [_table addInfiniteScrollingWithActionHandler:^{
        [self getData:YES];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _list.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return HEIGHT_CELL;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    Repository *repo = [_list objectAtIndex:indexPath.row];
    
    PullViewController *navigationController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"pull"];
    navigationController.repoName = repo.full_name;
    [self.navigationController pushViewController:navigationController animated:YES];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    Repository * repo;
    
    [_table registerNib:[UINib nibWithNibName:@"ItemListCell" bundle:nil]  forCellReuseIdentifier:@"cell"];
    ItemListTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    repo = [_list objectAtIndex:indexPath.row];
    [self configCell:cell repo:repo];
    
    return cell;
}

-(void)configCell:(ItemListTableViewCell *)c repo:(Repository *)repo{
    c.lblNameRepository.text = repo.name;
    c.lblDescRepository.text = repo.description;
    c.lblCountForksRepository.text = [NSString stringWithFormat: @"%i", repo.forks_count];
    c.lblCountStarsRepository.text = [NSString stringWithFormat: @"%i", repo.stargazers_count];
    c.lblNameUser.text = repo.owner.login;
    
    [c.imgAvatarUser sd_setImageWithURL:[NSURL URLWithString:repo.owner.avatar_url] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        if (error) {
            c.imgAvatarUser.image = [UIImage imageNamed:@"icon_profile_empty"];
        }
    }];
}

-(void)getData:(BOOL)nextPage{
    if ([Util internetConnected]){
        if (nextPage)
            page = page + 1;
        else{
            page = 1;
            [Util showLoading:self.view];
        }
        
        GithubService *service = [[GithubService alloc] initWithServiceListener:self];
        [service getJavaData:page];
    } else {
        [Util exibirAlerta:@"Ops.." mensagem:@"Verifique a conexão com a internet"];
        [self adjustControlVisibility:NO];
    }
}


-(void)onSuccess:(id)objetoRetorno{
    NSMutableArray *newList;
    [self adjustControlVisibility:YES];
    
    if ([objetoRetorno count] == 0 && page > 1) {
        [_table.pullToRefreshView stopAnimating];
        [_table.infiniteScrollingView stopAnimating];
        [Util hiddenLoading:self.view];
        return;
    }
    
    Response *r = [Response alloc];
    r = [objetoRetorno objectAtIndex:0];
    
    newList = [[NSMutableArray alloc] init];
    newList = [Repository arrayOfModelsFromDictionaries: [r.items copy] error:nil];
    
    if (page == 1)
        _list = newList;
    else
        [_list addObjectsFromArray:newList];
    
    [Util hiddenLoading:self.view];
    [_table reloadData];
    [_table.pullToRefreshView stopAnimating];
    [_table.infiniteScrollingView stopAnimating];
}

-(void)onError:(NSHTTPURLResponse*)response WithData:(NSData*)data WithError:(NSError*)error{
    [Util hiddenLoading:self.view];
    [Util exibirAlerta:@"Ops.." mensagem:@"Erro ao buscar os dados no servidor!"];
    [self adjustControlVisibility:NO];
    NSLog(@"Erro!");
}


- (IBAction)clickBtnTryAgain:(id)sender {
    [self adjustControlVisibility:YES];
    [self getData:NO];
}

-(void)adjustControlVisibility:(BOOL)showTable{
    if (showTable){
        _table.hidden = NO;
        _btnTryAgain.hidden = YES;
    } else {
        _table.hidden = YES;
        _btnTryAgain.hidden = NO;
    }
}

@end





















