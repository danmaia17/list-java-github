//
//  Response.h
//  TestConcrete
//
//  Created by Daniel Maia dos Passos on 10/01/17.
//  Copyright © 2017 Daniel Maia. All rights reserved.
//

#import "JSONModel.h"
#import "Repository.h"
#import <Foundation/Foundation.h>

@interface Response : JSONModel

@property (nonatomic, assign) double total_count;
@property (nonatomic, assign) BOOL incomplete_results;
@property (nonatomic, strong) NSArray<Repository *>* items;

@end
