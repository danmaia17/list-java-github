//
//  ServiceListener.h
//  TestConcrete
//
//  Created by Daniel Maia dos Passos on 10/01/17.
//  Copyright © 2017 Daniel Maia. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <JSONModel.h>

@protocol ServiceListener <NSObject>

- (void) onSuccess:(id)objetoRetorno;
- (void) onError:(NSHTTPURLResponse*)response WithData:(NSData*)data WithError:(NSError*)error;

@end
