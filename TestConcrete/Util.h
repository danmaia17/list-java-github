//
//  ClasseUtil.h
//  TestConcrete
//
//  Created by Daniel Maia dos Passos on 10/01/17.
//  Copyright © 2017 Daniel Maia. All rights reserved.
//


#import <UIKit/UIKit.h>


@interface Util : NSObject

+(BOOL)internetConnected;
+(void)showLoading:(UIView*) view;
+(void)hiddenLoading:(UIView*) view;
+(void)exibirAlerta:(NSString*)titulo mensagem:(NSString*)mensagem;

@end

